package com.example.broadcastreceivertest;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class PagerFragment extends Fragment implements TabListener,
		OnPageChangeListener {

	public static final String a = "falksjf";

	ViewPager pager;
	ActionBar actionBar;
	TextView tv;
	Handler handler = new Handler();
	Button btn;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		pager = (ViewPager) getActivity().findViewById(R.id.pager);
		MyPagerAdapter adapter = new MyPagerAdapter(getActivity()
				.getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setOnPageChangeListener(this);
		actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		Tab tab1 = actionBar.newTab().setText("tab1").setTabListener(this);
		Tab tab2 = actionBar.newTab().setText("tab2").setTabListener(this);
		Tab tab3 = actionBar.newTab().setText("tab3").setTabListener(this);
		actionBar.addTab(tab1);
		actionBar.addTab(tab2);
		actionBar.addTab(tab3);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment, container, false);
	}

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		pager.setCurrentItem(arg0.getPosition());
		run(arg0.getPosition());
	}
	int j;
	private void run(int i) {
		if (i == 2) {
			tv = (TextView) getActivity().findViewById(R.id.text_fragC);
			if (tv == null)
				Log.d("TAG", "NULL");
			else {
				tv.setText("OnTabSelected fragC");
				Log.d("TAG", "NOT NULL");
				/*new Thread(new Runnable(){
					public void run() {
						for (int i = 0; i < 100; i++) {
							final int k = i;
							handler.post(new Runnable() {
								public void run() {
									tv.setText(k + "");
								}
							});
							SystemClock.sleep(100);
						}
					}
				}).start();*/
				final Handler handler = new Handler();
				j = 0;
				handler.post(new Runnable() {
					public void run() {
						if (j < 100) {
							j++;
							//tv.setText(j + "");
							handler.postDelayed(this, 100);
						}
					}
				});
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", "onDestroy Pager");
		actionBar.removeAllTabs();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		run(actionBar.getSelectedTab().getPosition());
		Log.d("TAG", "onResume Pager");
	}
	
	

	/*
	 * new Thread(new Runnable() { public void run() { for (int i = 0; i < 50;
	 * i++) { final int k = i; handler.post(new Runnable(){ public void run() {
	 * tv.setText(k + ""); } }); SystemClock.sleep(100); } } }).start();
	 */
	// }
	/*
	 * if (arg0.getPosition() == 2) { new Thread(new Runnable(){ public void
	 * run() { for (int i = 0; i < 50; i++) { final int k = i; handler.post(new
	 * Runnable(){ public void run() { tv.setText(k + ""); } });
	 * SystemClock.sleep(100); } } }).start(); }
	 */
	// }

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
	}

	public class MyPagerAdapter extends FragmentStatePagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			Fragment f = null;
			if (arg0 == 0)
				f = new FragmentA();
			else if (arg0 == 1)
				f = new FragmentB();
			else
				f = new FragmentC();
			return f;
		}

		@Override
		public int getCount() {
			return 3;
		}

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		actionBar.setSelectedNavigationItem(arg0);
	}

}
