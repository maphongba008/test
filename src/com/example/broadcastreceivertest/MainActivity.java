package com.example.broadcastreceivertest;

import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

public class MainActivity extends ActionBarActivity {

	BroadcastReceiver receiver = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null)
		getSupportFragmentManager().beginTransaction()
				.add(R.id.container, new FirstFragment(), "").commit();

	}
}
