package com.example.broadcastreceivertest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FirstFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("TAG", "onCreateView FirstFragment");
		return inflater.inflate(R.layout.first_fragment, container, false);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", "onDestroy FirstFragment");
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("TAG", "onActivityCreated FirstFragment");
		Button btn = (Button) getActivity().findViewById(R.id.btntosecond);
		btn.setText("Move to next");
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new SecondFragment(), "")
				.addToBackStack("").commit();
			}
		});
	}
	
}
