package com.example.broadcastreceivertest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class SecondFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("TAG", "onCreateView SecondFragment");
		return inflater.inflate(R.layout.second_fragment, container, false);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", "onDestroy SecondFragment");
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("TAG", "onActivityCreated SecondFragment");
		Button btn = (Button) getActivity().findViewById(R.id.btntoview);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getActivity().getSupportFragmentManager()
				.beginTransaction().replace(R.id.container, new PagerFragment(), "")
				.addToBackStack("").commit();
			}
		});
	}
}
