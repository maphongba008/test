package com.example.broadcastreceivertest;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentC extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("TAG", "onCreateView FragmentC");
		return inflater.inflate(R.layout.fragment_c, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("TAG", "onActivityCreated FragmentC");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", "onDestroy FragmentC");
	}
	
}
