package com.example.broadcastreceivertest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentA extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("TAG", "onCreateView FragmentA");
		return inflater.inflate(R.layout.fragment_a, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("TAG", "onActivityCreated FragmentA");
		TextView tv = (TextView) getActivity().findViewById(R.id.text_fragA);
		tv.setText("This is framentA");
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", "onDestroy FragmentA");
	}
	
}
